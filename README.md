# Dialogs For All!

Makes it possible to use query parameters display any (internal) link in a "popup" dialog.
If the dialog is declared as *modal*, it blocks the rest of the page' function.
Note that all query parameters under the "dialog" namespace are converted to
internal attributes and do not show up in the final link.

## Basic syntax

The simpest form just opens a dialog:

* [internal:/?dialog](internal:/?dialog)

...which is the short form of

* [internal:/?dialog=dialog](internal:/?dialog=dialog)

The only other built in value for type is *modal*. Contributed modules may add to this.

* [internal:/?dialog=modal](internal:/?dialog=modal)

## Extended syntax

### Dialog type

The above is equivalent to:

* [internal:/?dialog[type]=dialog](internal:/?dialog[type]=dialog)

...and:

* [internal:/?dialog[type]=modal](internal:/?dialog[type]=modal)

In other words, basic syntax only allows to specify dialog type.
So let's see what there's more.

### Dialog renderer

You can also use: 

* [internal:/?dialog[renderer]=off_canvas](internal:/?dialog[renderer]=off_canvas)

...and:

* [internal:/?dialog[renderer]=off_canvas_top](internal:/?dialog[renderer]=off_canvas_top)

...and of course combine this with dialog type:

* [internal:/?dialog[type]=modal&dialog[renderer]=off_canvas](internal:/?dialog[type]=modal&dialog[renderer]=off_canvas)

These are currently the only built in renderers. Contributed modules may add to this.

### Dialog options

You can use all valid [JQuery UI dialog options](https://api.jqueryui.com/dialog/).

Either as array:

* [internal:/?dialog[options][height]=100&dialog[options][width]=200](internal:/?dialog[options][height]=100&dialog[options][width]=200)

...or as JSON string:

* [internal:/?dialog[options]={height:100,width:200}](internal:/?dialog[options]={height:100,width:200})

### Effects

You can use effects for the *show* and *hide* options:

* [internal:/?dialog[options]={show:{effect:"slideUp",duration:"800"},hide:{effect:"slideDown",duration:"800"}}](internal:/?dialog[options]={show:{effect:"slideUp",duration:"800"},hide:{effect:"slideDown",duration:"800"}})

### Libraries

You can require additional libraries, e.g. for effects:

* [internal:/?dialog[options]={hide:{effect:"explode"}}&dialog[libraries][]=jquery.ui.effects.explode](internal:/?dialog[options]={show:{effect:"explode"}}&dialog[libraries][]=jquery.ui.effects.explode)
