<?php

namespace Drupal\dialogs;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Render\Renderer;

class DialogifyerFactory {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * The library discovery.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * The query key, hardcoded for now.
   *
   * @var string
   */
  protected $queryKey = 'dialog';

  /**
   * DialogifyerFactory constructor.
   *
   * @param \Drupal\Core\Render\Renderer $renderer
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $libraryDiscovery
   */
  public function __construct(Renderer $renderer, LibraryDiscoveryInterface $libraryDiscovery) {
    $this->renderer = $renderer;
    $this->libraryDiscovery = $libraryDiscovery;
  }

  /**
   * Parse the query.
   *
   * @param array|NULL $query
   *   THe query.
   *
   * @return \Drupal\dialogs\Dialogifyer|null
   *  The dialogifyer object.
   */
  public function fromQueryArray(array $query = NULL) {
    if (isset($query[$this->queryKey])) {
      $spec = $query[$this->queryKey];
      if (!is_array($spec)) {
        $type = $spec;
        $renderer = NULL;
        $options = NULL;
        $libraries = [];
      }
      else {
        $type = $spec['type'] ?? NULL;
        $renderer = $spec['renderer'] ?? NULL;
        $options = $spec['options'] ?? NULL;
        $libraries = $spec['libraries'] ?? [];
      }
      $type = $type ?: 'dialog';
      if (!is_array($libraries)) {
        $libraries = explode('|', $libraries);
      }
      $libraries = array_filter($libraries, function ($library) {
        $parts = explode('/', $library);
        if (count($parts) !== 2) {
          return FALSE;
        }
        list($extension, $name) = $parts;
        return $this->libraryDiscovery->getLibraryByName($extension, $name);
      });
      return new Dialogifyer($type, $renderer, $options, $libraries, [$this->queryKey], $this->renderer);
    }
    else {
      return NULL;
    }
  }

}
