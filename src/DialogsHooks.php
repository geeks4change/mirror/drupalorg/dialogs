<?php

namespace Drupal\dialogs;

class DialogsHooks {

  /**
   * The dialogifyer factory.
   *
   * @var \Drupal\dialogs\DialogifyerFactory
   */
  protected $dialogifyerFactory;

  /**
   * DialogsPathProcessor constructor.
   *
   * @param \Drupal\dialogs\DialogifyerFactory $dialogifyerFactory
   */
  public function __construct(DialogifyerFactory $dialogifyerFactory) {
    $this->dialogifyerFactory = $dialogifyerFactory;
  }

  /**
   * Delegated hook_link_alter()..
   *
   * Rewrite dialogified links.
   * Unfortunately we can not use an OutboundPathProcessor, as it only handles the
   * URL and forgets about link attributes. Sigh.
   * Also we have to use the early-rendering hack in the dialogifyer for the
   * attached libraries, as we do not have metadata in variables. Sigh.

   * @throws \Exception
   */
  public function hookLinkAlter(&$variables) {
    $dialogifyer = $this->dialogifyerFactory->fromQueryArray($variables['options']['query'] ?? NULL);
    if ($dialogifyer) {
      $dialogifyer->alterLinkOptions($variables['options']);
    }
  }

}
